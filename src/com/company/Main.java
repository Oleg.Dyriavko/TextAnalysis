package com.company;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class Main {

    // get list from a file
    public static List<String> getWordsList() throws IOException {
        List<String> strings = new ArrayList<>();
        Path path = Paths.get("./Text.txt");
        Files.readAllLines(path)
                .forEach(word -> strings.addAll(Arrays.asList(word.split(" "))));
        return strings;

    }

    public static void searchAndCountWords() throws IOException {
        List<String> wordsList = getWordsList();

        int count;

        for (String word : wordsList) {
            count = 0;
            for (String searchWord : wordsList) {
                if (word.equals(searchWord)) {
                    count++;
                }
            }

            System.out.println("Word " + word + " appears " + count + " times");
        }
        System.out.println("=================================");
    }


    public static void sortWordsAlphabetically() throws IOException {
        List<String> stringList = getWordsList();

        System.out.println(stringList.stream().sorted().collect(Collectors.toList()));
        System.out.println("=================================");
    }

    public static void removeShortWords() throws IOException {
        List<String> stringList = getWordsList();

        System.out.println(stringList.stream().filter(element -> element.length()>3).collect(Collectors.toList()));
        System.out.println("=================================");
    }

    public static void main(String[] args) throws IOException {
        searchAndCountWords();
        sortWordsAlphabetically();
        removeShortWords();
    }
}
